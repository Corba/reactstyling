import React from "react";
import Contactmoments from "../components/Contactmoments.js";
import TitleBlock from "../components/TitleBlock.js";

export default React.createClass({
  getInitialState() {
    return {
      contactmoments: []
    }
  },

  componentDidMount() {
    $.ajax({
      url: "/style",
      dataType: "json",
      success: this.handleResponse
    });
  },

  handleResponse(response) {
    this.setState({contactmoments: response.contactmoments});
  },

  findIndex(array, id) {
    array.forEach(function(c, index) {
      if(c.id == id) {
        return index;
      }
    });
  },

  handleChange(id, newValue) {
    const contactmoments = Object.assign([], this.state.contactmoments);
    contactmoments[id] = Object.assign({}, contactmoments[id], newValue);
    this.setState({contactmoments: contactmoments});
  },

  render() {
    return (
      <div className="content">
        <TitleBlock />
        <Contactmoments
          contactmoments={this.state.contactmoments}
          onChange={this.handleChange} />
      </div>
    );
  }
});
