import React from "react";

export default React.createClass({
  render() {
    return (
      <div className="navigation">
        <img className="logo" src="/gridline.png" />
        <div className="navigation-items">
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-users "></i>
            </div>
            Klantbeheer
          </div>
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-lightbulb-o"></i>
            </div>
            WBSO
          </div>
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-files-o"></i>
            </div>
            Contactmomenten
          </div>
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-phone"></i>
            </div>
            Vervolgafspraken
          </div>
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-handshake-o"></i>
            </div>
            Deals
          </div>
          <div className="navigation-item">
            <div className="navigation-icon">
              <i className="fa fa-paper-plane-o"></i>
            </div>
            Mailinglists
          </div>
        </div>
      </div>
    );
  }
});
