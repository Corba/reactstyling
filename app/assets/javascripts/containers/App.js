import React from "react";
import PageContainer from "../containers/PageContainer.js";
import Navigation from "../containers/Navigation.js";

export default React.createClass({
  render() {
    return (
      <div>
        <Navigation />
        <PageContainer />
      </div>
    );
  }
});
