import React from "react";
import ExpandedContactmoment from "../components/expanded_contactmoment.js"

const Contactmoment =  (props) => {
  const handleExpand = () => {
    props.onChange(props.id, { expanded: !props.expanded})
  };

  const expandClass = () => {
    return props.expanded ? "down" : "left"
  };

  return (
    <div className="contactmoment">
      <div className="company">
        {props.company}
      </div>
      <div className="contact">
        {props.contact}
      </div>
      <div className="product">
        {props.product}
      </div>
      <div className="expand-action" onClick={handleExpand}>
        <i className={`fa fa-chevron-${expandClass()}`}></i>
      </div>

      {props.expanded &&
        <ExpandedContactmoment expandText={props.expandText} />
      }
    </div>
  );
};

Contactmoment.propTypes = {
  id: React.PropTypes.number,
  company: React.PropTypes.string,
  contact: React.PropTypes.string,
  product: React.PropTypes.string,
  expandText: React.PropTypes.string,
  expanded: React.PropTypes.bool,
  onChange: React.PropTypes.func
};

export default Contactmoment;
