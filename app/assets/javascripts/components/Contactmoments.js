import React from "react";
import Contactmoment from "../components/Contactmoment.js";
import Headers from "../components/Headers.js";

const Contactmoments = (props) =>{
  return (
    <div className="contactmoments-block">
      <Headers />
      <div className="contactmoments">
        {props.contactmoments.map((contactmoment) =>
          <Contactmoment
            key={contactmoment.id}
            onChange={props.onChange}
            {...contactmoment} />
        )}
      </div>
    </div>
  );
};

Contactmoments.propTypes = {
  contactmoments: React.PropTypes.array,
  onChange: React.PropTypes.func
}

export default Contactmoments;
