import React from "react";

const ExpandedContactmoment = (props) => {
  return (
    <div className="expanded-contactmoment">
      {props.expandText}
    </div>
  );
};

ExpandedContactmoment.propTypes = {
  expandText: React.PropTypes.string
}

export default ExpandedContactmoment;
