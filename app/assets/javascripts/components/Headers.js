import React from "react";

const Headers = () => {
  return (
    <div className="headers">
      <div className="header">
        Bedrijf
      </div>
      <div className="header">
        Contactpersoon
      </div>
      <div className="header">
        Product
      </div>
    </div>
  );
};

export default Headers;
