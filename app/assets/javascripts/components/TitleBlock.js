import React from "react";

const TitleBlock = () => {
  return (
    <div className='title-block'>
      <div className='title'> Contactmomenten </div>
      <div className='add'>
        <i className='fa fa-plus-circle'></i>
      </div>
    </div>
  )
};

export default TitleBlock;
