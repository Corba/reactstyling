class StyleController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.json do
        render json: { contactmoments: contactmoments }
      end
    end
  end

  private

  def contactmoments
    contactmoments = []

    for i in 0..15
      contactmoments << {
        id: i,
        company: Faker::Company.name,
        contact: Faker::Name.name,
        product: Faker::Commerce.product_name,
        expandText: Faker::Lorem.paragraphs(8).join(',').gsub!(',', ' ')
      }
    end

    return contactmoments
  end
end
