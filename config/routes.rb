Rails.application.routes.draw do
  resources :style, only: :index

  root to: 'style#index'
end
