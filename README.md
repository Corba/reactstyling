# README

**Ruby Version**
2.2.3

**Rails Version**
5.0.0.1

**NPM Version**
2.15

**Startup commands**

`WEBPACK_DEV_SERVER=http://localhost:8080/index.js bundle exec rails s`

`./node_modules/webpack-dev-server/bin/webpack-dev-server.js --content-base app/assets/javascripts/.generated/ --lazy --devtool inline-source-map`