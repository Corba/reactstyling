var path = require('path');
var webpack = require('webpack');

var config = module.exports = {
  entry: path.resolve(__dirname, 'app/assets/javascripts/index.js'),
  output: {
    path: path.resolve(__dirname, 'app/assets/javascripts/.generated'),
    filename: 'index.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, 'app/assets/javascripts')
        ],
        loader: 'babel',
        query: {
          cacheDirectory: true
        }
      },
      {
        test: /\.jsx$/,
        loader: 'babel',
        query: {
          cacheDirectory: true,
          babelrc: false,
          presets: [
            require.resolve('babel-preset-es2015'),
            require.resolve('babel-preset-react')
          ]
        }
      }
    ]
  },
};
